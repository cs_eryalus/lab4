/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

import com.google.bitcoin.core.Sha256Hash;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.SecretKey;
import org.apache.commons.lang3.ArrayUtils;
import sun.security.util.ArrayUtil;

/**
 *
 * @author eryalus
 */
public class treeUtils {

    /**
     *
     * @param n
     * @return
     */
    public static int getSibling(int n) {
        if (n % 2 == 0) {
            return n + 1;
        } else {
            return n - 1;
        }
    }

    /**
     *
     * @param n
     * @return
     */
    public static int getParent(int n) {
        if (n % 2 == 0) {
            return n / 2;
        } else {
            return (n - 1) / 2;
        }
    }

    /**
     *
     * @param n
     * @return
     */
    private static int getFirstChild(int n) {
        return n * 2;
    }

    public static ArrayList<HashWithIndex> getNeededHashesToVerify(Hash[] keys, int indexOfKnownHash) {
        boolean[] avoid = new boolean[keys.length];
        int temp = indexOfKnownHash;
        while (temp > 0) {
            avoid[temp - 1] = true;
            temp /= 2;
        }
        ArrayList<HashWithIndex> calculateBranch = calculateBranch(keys, avoid, 1);
        calculateBranch.add(new HashWithIndex(keys[indexOfKnownHash - 1], indexOfKnownHash));
        return calculateBranch;
    }

    public static HashMap<Integer, Hash> getTreeNodeCountFromHashCount(List<Sha256Hash> hashes) {
        HashMap<Integer, Hash> hashMap = new HashMap<>();
        //create tree base
        double logD = (Math.log(hashes.size()) / Math.log(2));
        int log = (int) (Math.log(hashes.size()) / Math.log(2));
        if (Math.pow(2, logD) != Math.pow(2, log)) {
            log++;
        }
        int index = (int) Math.pow(2, log);
        int rowSize = index;
        int hashCount = hashes.size();
        Hash[] toDuplicate = new Hash[2];
        if (hashCount % 2 != 0) {
            byte[] bytes = ArrayUtils.clone(hashes.get(hashCount - 1).getBytes());
            ArrayUtils.reverse(bytes);
            toDuplicate[0] = new Hash(bytes);
            toDuplicate[1] = new Hash(bytes);
        } else {
            byte[] bytes = ArrayUtils.clone(hashes.get(hashCount - 2).getBytes());
            byte[] bytes2 = ArrayUtils.clone(hashes.get(hashCount - 1).getBytes());
            ArrayUtils.reverse(bytes);
            ArrayUtils.reverse(bytes2);
            toDuplicate[0] = new Hash(bytes);
            toDuplicate[1] = new Hash(bytes2);
        }
        for (int i = 0; i < rowSize; i++) {
            if (i < hashCount) {
                byte[] bytes = ArrayUtils.clone(hashes.get(i).getBytes());
                ArrayUtils.reverse(bytes);
                hashMap.put(index + i, new Hash(bytes));
            } else {
                if (i % 2 != 0) {
                    hashMap.put(index + i, toDuplicate[1]);
                } else {
                    hashMap.put(index + i, toDuplicate[0]);
                }
            }
        }
        //tree base created
        //fill the tree upwards
        for (int i = (log - 1); i >= 0; i--) {
            int startIndex = (int) Math.pow(2, i);
            int endIndex = (int) Math.pow(2, i + 1);
            while (startIndex < endIndex) {
                Sha256Hash hash = doubleHash(hashMap.get(((int) (startIndex * 2))).getHashValue(), hashMap.get(((int) (startIndex * 2)) + 1).getHashValue());
                hashMap.put(startIndex, new Hash(hash.getBytes()));
                startIndex++;
            }
        }
        return hashMap;
    }

    public static Sha256Hash doubleHash(byte[] b0, byte[] b1) {
        return Sha256Hash.create(Sha256Hash.create(ArrayUtils.addAll(b0, b1)).getBytes());
    }

    private static ArrayList<HashWithIndex> calculateBranch(Hash[] keys, boolean[] brokenNodes, int counter) {
        ArrayList<HashWithIndex> secretKeys = new ArrayList<>();
        if ((counter - 1) < keys.length) {
            if (brokenNodes[counter - 1] && ((counter) % 2 == 0)) {
                secretKeys.addAll(calculateBranch(keys, brokenNodes, counter + 1));
                counter = getFirstChild(counter);
                secretKeys.addAll(calculateBranch(keys, brokenNodes, counter));
            } else if (brokenNodes[counter - 1]) {
                counter = getFirstChild(counter);
                secretKeys.addAll(calculateBranch(keys, brokenNodes, counter));
            } else if (counter % 2 == 0) {
                HashWithIndex key_with_index = new HashWithIndex(keys[counter - 1], counter);
                secretKeys.add(key_with_index);
                secretKeys.addAll(calculateBranch(keys, brokenNodes, counter + 1));
            } else {
                HashWithIndex key_with_index = new HashWithIndex(keys[counter - 1], counter);
                secretKeys.add(key_with_index);
            }
        }
        return secretKeys;
    }

    public static ArrayList<Integer> getListOfAvoidingNodes(int device) {
        ArrayList<Integer> list = new ArrayList<>();
        int index = device;
        list.add(index);
        while (index > 1) {
            index = getParent(index);
            list.add(index);
        }
        return list;
    }

    public static int[] getDeviceKeys(int device, int deviceCount, int nodeCount) {
        int symbolicID = nodeCount - deviceCount + device + 1;
        int keyCount = (int) (Math.log(symbolicID) / Math.log(2)) + 1;
        int[] deviceKeys = new int[keyCount];
        int pointer = symbolicID;
        for (int i = 0; i < keyCount; i++) {
            deviceKeys[i] = symbolicID;
            symbolicID = ((int) symbolicID / 2);
        }
        return deviceKeys;
    }
}
