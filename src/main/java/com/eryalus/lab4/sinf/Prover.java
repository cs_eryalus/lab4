/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

import com.google.bitcoin.core.Block;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.core.Sha256Hash;
import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.params.MainNetParams;
import com.google.bitcoin.utils.BlockFileLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author eryalus
 */
public class Prover {

    private HashMap<Integer, Hash[]> merkleTrees = new HashMap<>();

    public void createMerkleTress() {
        System.out.println("Opening file...");
        NetworkParameters np = new MainNetParams();
        List<File> blockChainFiles = new ArrayList<>();
        blockChainFiles.add(new File(Constants.BLOCKS_PATH));
        BlockFileLoader bfl = new BlockFileLoader(np, blockChainFiles);
        System.out.println("Creating merkle trees...");
        long initTime = System.currentTimeMillis();
        int counter = 0;
        for (Block block : bfl) {

            ArrayList<Sha256Hash> hashes = new ArrayList<>();
            for (Transaction t : block.getTransactions()) {
                hashes.add(t.getHash());
            }
            HashMap<Integer, Hash> treeNodeCountFromHashCount = treeUtils.getTreeNodeCountFromHashCount(hashes);
            Hash[] hashHashes = new Hash[treeNodeCountFromHashCount.keySet().size()];
            ArrayList<Integer> keys = new ArrayList<>();
            for (int k : treeNodeCountFromHashCount.keySet()) {
                keys.add(k);
            }
            Collections.sort(keys);
            for (Integer key : keys) {
                hashHashes[key - 1] = treeNodeCountFromHashCount.get(key);
            }
            merkleTrees.put(counter++, hashHashes);
        }
        long time = System.currentTimeMillis() - initTime;
        System.out.println(counter + " Merkle trees created in " + time + "ms");
    }

    public int getBlockCount() {
        return merkleTrees.size();
    }

    public Hash[] getLeafHashesOfBlock(int index) {
        Hash[] hashes = merkleTrees.get(index);
        int baseIndex = (hashes.length + 1) / 2;
        Hash[] returnHashes = Arrays.copyOfRange(hashes, baseIndex - 1, hashes.length);
        return returnHashes;
    }

    public ProverResponse getHashesToValidateHash(int block, String hash) {
        Hash[] hashes = merkleTrees.get(block);
        int baseIndex = (hashes.length + 1) / 2;
        Hash[] returnHashes = Arrays.copyOfRange(hashes, baseIndex - 1, hashes.length);
        int index = -1;
        int counter = 0;
        for (Hash h : returnHashes) {
            if (Transforms.bytesToHex(h.getHashValue()).toUpperCase().equals(hash.toUpperCase())) {
                index = counter + baseIndex;
                break;
            }
            counter++;
        }
        if (index == -1) {
            return new ProverResponse(null, -1);
        }
        return new ProverResponse(treeUtils.getNeededHashesToVerify(merkleTrees.get(block), index), index);
    }
}
