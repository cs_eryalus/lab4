/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

import java.util.Scanner;

/**
 *
 * @author eryalus
 */
public class Main {

    public static void main(String[] args) {
        Prover prover = new Prover();
        //create and save in memory a merkle tree per block
        prover.createMerkleTress();
        //just for avoiding re-reading the blockchain file
        Verifier verifier = new Verifier(prover);
        int blockCount = prover.getBlockCount();
        int selectedBlock;
        int counter;
        String hash;
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("Select a block <0-" + (blockCount - 1) + ">");
            try {
                selectedBlock = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException ex) {
                continue;
            }
            if (selectedBlock < 0 || selectedBlock >= blockCount) {
                continue;
            }
            Hash[] hashes = prover.getLeafHashesOfBlock(selectedBlock);
            counter = 0;
            for (Hash h : hashes) {
                System.out.println("Trans." + counter++ + " - " + Transforms.bytesToHex(h.getHashValue()));
            }
            System.out.print("Insert transaction hash: ");
            hash = scan.nextLine().trim();
            System.out.println("Block contains transaction? " + verifier.verifyTransactionOfBlock(hash, selectedBlock));
        }
    }

}
