/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class ProverResponse {

    private ArrayList<HashWithIndex> hashes;
    private int hashIndex;

    public ProverResponse(ArrayList<HashWithIndex> hashes, int hashIndex) {
        this.hashes = hashes;
        this.hashIndex = hashIndex;
    }

    public ArrayList<HashWithIndex> getHashes() {
        return hashes;
    }

    public void setHashes(ArrayList<HashWithIndex> hashes) {
        this.hashes = hashes;
    }

    public int getHashIndex() {
        return hashIndex;
    }

    public void setHashIndex(int hashIndex) {
        this.hashIndex = hashIndex;
    }

}
