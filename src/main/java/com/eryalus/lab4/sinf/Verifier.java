/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

import com.google.bitcoin.core.Block;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.params.MainNetParams;
import com.google.bitcoin.utils.BlockFileLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author eryalus
 */
public class Verifier {

    private Prover prover;

    public Verifier(Prover prover) {
        this.prover = prover;
    }

    private Hash getHashByIndex(int index, ArrayList<HashWithIndex> hashes) {
        for (HashWithIndex h : hashes) {
            if (h.getIndex() == index) {
                return h.getHash();
            }
        }
        return null;
    }

    public boolean verifyTransactionOfBlock(String hash, int block) {
        boolean inSet = false;
        ProverResponse response = prover.getHashesToValidateHash(block, hash);
        ArrayList<HashWithIndex> hashesToValidateHash = response.getHashes();
        if (hashesToValidateHash != null) {
            if (hashesToValidateHash.isEmpty()) {
                inSet = true;
            } else {
                int hashIndex = response.getHashIndex();
                int sibling;
                byte[] temporaryHash = getHashByIndex(hashIndex, hashesToValidateHash).getHashValue();
                while (hashIndex > 1) {
                    sibling = treeUtils.getSibling(hashIndex);
                    if (hashIndex % 2 == 0) {
                        temporaryHash = treeUtils.doubleHash(temporaryHash, getHashByIndex(sibling, hashesToValidateHash).getHashValue()).getBytes();
                    } else {
                        temporaryHash = treeUtils.doubleHash(getHashByIndex(sibling, hashesToValidateHash).getHashValue(), temporaryHash).getBytes();
                    }
                    hashIndex = treeUtils.getParent(hashIndex);
                }
                //temporaryHash contains the merkle root
                System.out.println("Calculated root: " + Transforms.bytesToHex(temporaryHash));
                System.out.println("Find merkle root from block...");
                System.out.println("Opening file...");
                NetworkParameters np = new MainNetParams();
                List<File> blockChainFiles = new ArrayList<>();
                blockChainFiles.add(new File(Constants.BLOCKS_PATH));
                BlockFileLoader bfl = new BlockFileLoader(np, blockChainFiles);
                byte[] blockMerkleRoot = null;
                int counter = 0;
                for (Block blk : bfl) {
                    if (counter++ < block) {
                        continue;
                    }
                    blockMerkleRoot = ArrayUtils.clone(blk.getMerkleRoot().getBytes());
                    ArrayUtils.reverse(blockMerkleRoot);
                    break;
                }
                if (blockMerkleRoot == null) {
                    System.out.println("Block not found.");
                    inSet = false;
                } else {
                    System.out.println("Found merkle root: " + Transforms.bytesToHex(blockMerkleRoot));
                    inSet = Transforms.bytesToHex(temporaryHash).equals(Transforms.bytesToHex(blockMerkleRoot));
                }
            }
        }
        return inSet;
    }
}
