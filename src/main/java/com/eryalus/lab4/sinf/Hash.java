/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

/**
 *
 * @author eryalus
 */
public class Hash {

    private byte[] hashValue = null;

    public Hash(byte[] hash) {
        hashValue = hash;
    }

    public byte[] getHashValue() {
        return hashValue;
    }

    public void setHashValue(byte[] hashValue) {
        this.hashValue = hashValue;
    }

}
