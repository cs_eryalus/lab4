/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.lab4.sinf;

/**
 *
 * @author eryalus
 */
public class HashWithIndex {

    int index = -1;
    Hash hash = null;

    public HashWithIndex(Hash hash, int index) {
        this.hash = hash;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Hash getHash() {
        return hash;
    }

    public void setHash(Hash hash) {
        this.hash = hash;
    }

}
